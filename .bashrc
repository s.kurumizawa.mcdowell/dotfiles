#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

# Load WAL colors
# -- https://github.com/dylanaraps/pywal/wiki/Getting-Started#applying-the-theme-to-new-terminals
source ~/.cache/wal/colors-tty.sh	# For TTYs
cat ~/.cache/wal/sequences		# For terminal emulators

# Set custom prompt
PS1='\[\e[0;31m\]\u@\h \w\[\e[0m\] \$ '

# Watch youtube videos
yt() {
	youtube-dl "$1" -o - | mpv --fs -
}

# Set wallpaper (with pywal)
wp() {
	wal -i "$1"
}

# Pacman aliases
alias pac-add='sudo pacman -S'
alias pac-find='sudo pacman -Ss'
alias pac-rm='sudo pacman -R'

# Shortcuts for configuration files
alias conf-i3='nvim ~/.config/i3/config'
alias conf-bash='nvim ~/.bashrc && source .bashrc' # Autoreload changes
alias shutdown='shutdown now' # Don't wait one minute before shutdown
