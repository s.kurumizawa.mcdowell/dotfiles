call plug#begin('~/.config/nvim/plugged')
Plug 'junegunn/goyo.vim'
Plug 'nanotech/jellybeans.vim'
Plug 'itchyny/lightline.vim'
call plug#end()

set number
set linebreak
set noshowmode " For lightline

colors jellybeans

